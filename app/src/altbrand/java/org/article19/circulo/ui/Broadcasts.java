package org.article19.circulo.ui;


/**
 * Created by N-Pex on 2018-11-07.
 */

public class Broadcasts {
    public static final String BROADCAST_STATUS_UPDATE_CHANGED = "STATUS_UPDATE_CHANGED";

    public static final String EXTRAS_CONTACT_ID = "contact_id";
}
