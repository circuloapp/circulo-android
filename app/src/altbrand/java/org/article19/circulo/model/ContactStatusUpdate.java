package org.article19.circulo.model;

import android.text.TextUtils;

import java.util.Date;

/**
 * Created by N-Pex on 2018-10-30.
 */
public class ContactStatusUpdate {
    private Date date;
    private int emoji;
    private String message;
    private boolean urgent;
    private String location;


    private String messageId;

    private String replyId;

    // Has the user "seen" this update?
    private boolean seen;

    private int deliveryStatus;

    public final static int STATUS_QUEUED = 0;
    public final static int STATUS_SENT = 1;
    public final static int STATUS_DELIVERED = 2;

    public int getDeliveryStatus () { return deliveryStatus; }

    public void setDeliveryStatus (int deliveryStatus)
    {
        this.deliveryStatus = deliveryStatus;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getEmoji() {
        return emoji;
    }

    public void setEmoji(int emoji) {
        this.emoji = emoji;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isUrgent() {
        return urgent;
    }

    public void setUrgent(boolean urgent) {
        this.urgent = urgent;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    public boolean isEmojiOnlyUpdate() {
        return TextUtils.isEmpty(getMessage());
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }


    public String getReplyId() {
        return replyId;
    }

    public void setReplyId(String replyId) {
        this.replyId = replyId;
    }


}
